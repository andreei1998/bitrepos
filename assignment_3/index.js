
const FIRST_NAME = "Vulpe";
const LAST_NAME = "Andrei";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
class Employee {
    constructor(_name, _surname, _salary) {
        this.name = _name;
        this.surname = _surname;
        this.salary = _salary;
    }


    getDetails() {

        return this.name + ' ' + this.surname + ' ' + this.salary;
    }

}


class SoftwareEngineer extends Employee {
    constructor(name, surname, salary, _experience) {
        super(name, surname, salary);
        if (_experience === undefined) {
            this.experience = 'JUNIOR'
        }
        else
            this.experience = _experience;
    }
    applyBonus() {
        switch (this.experience) {
            case 'JUNIOR':
                return this.salary + this.salary * 10 / 100;
            case 'MIDDLE':
                 return this.salary + this.salary * 15 / 100;
            case 'SENIOR':
                return this.salary + this.salary * 20 / 100;
              
        }
        return this.salary + this.salary * 10 / 100;
    }
}
module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

