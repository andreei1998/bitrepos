
const FIRST_NAME = "Vulpe";
const LAST_NAME = "Andrei";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache = {};
  
    return {
      pageAccessCounter(key = "home") {
        key = key.toLowerCase();
        if(!cache.hasOwnProperty(key)) {
          cache[key]=1;
        }
        else {
			cache[key]++;
		}
      },
      getCache: () => cache,
    };
  }

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

